import { Component, OnInit } from '@angular/core';
import { Usuario } from '../Usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit{
    
    current_logi: Usuario;
    public usuari : string;
    public contrase : string;
    listUsu = [];
    valid: Boolean;
    validarlog: Boolean; 
   
    constructor(private router: Router){

    }

    grabar_localstorage(){

    	this.valid = true;
    	  var usuar = new Usuario(this.usuari, this.contrase);
          if (JSON.parse( localStorage.getItem("Usuario"))) {
              this.listUsu = JSON.parse( localStorage.getItem("Usuario"));
              for (var i = 0 ; i <this.listUsu.length; i++) {
              this.current_logi = this.listUsu[i];
                  if(this.current_logi.usuario == (this.usuari)){
                       alert("Lo siento el usuario ya existe");
                       this.valid = false;
                       break 
                  }
           }
          }    	  
    	  
    	  	if(this.valid){
    	  		 this.listUsu.push(usuar);
          		 localStorage.setItem( "Usuario", JSON.stringify(this.listUsu));
          		 alert("Registrado correcto");
    	  	}
    }

    login_localstorage(){
    	  this.validarlog=false;
        if (JSON.parse( localStorage.getItem("Usuario"))) {

          this.listUsu = JSON.parse( localStorage.getItem("Usuario"));
          for (var i = 0 ; i <this.listUsu.length; i++) {
            this.current_logi = this.listUsu[i];
              if(this.current_logi.usuario == (this.usuari)){
                 if(this.current_logi.contrseña == this.contrase){
                     this.validarlog = true;
                     break;
                 }
              }
           }

        }
    	   if(this.validarlog){
    	   		alert("Iniciando Album......."); 
            this.router.navigate(['principal']);
    	   }else{
    	   	alert("El usuario ingresado no existe o la contraseña es incorrecta");
    	   }
    }
    
    
    ngOnInit() {

	}
	
}